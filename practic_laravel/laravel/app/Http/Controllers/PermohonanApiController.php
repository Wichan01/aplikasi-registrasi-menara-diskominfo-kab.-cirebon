<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Permohonan;
use File;

class PermohonanApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permohonans = Permohonan::all()->toJson(JSON_PRETTY_PRINT);
        return response($permohonans, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'Instansi'                   => 'required|min:3|max:100',
            'Latitude'                   => 'required',
            'Longitude'                  => 'required',
            'Dokumen Permohonan'         => 'required|file|doc|max:2000'
        ]);
        if($validateData->fails()) {
            return response($validateData->errors(), 400);
        }else{
            $perusahaan = new Permohonan();
        $perusahaan->Instansi = $validateData['Instansi'];
        $perusahaan->Latitude = $validateData['Latitude'];
        $perusahaan->Longitude = $validateData['Longitude'];

        if ($request->hasFile('doc')) {
            $extFile = $request->doc->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->doc->move('assets/dokumen', $namaFile);
            $perusahaan->doc = $path;
        }
    
            $perusahaan->save();
            return response()->json(["message"=>"permohonan record created"], 201);}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Profile;
use App\Models\Permohonan;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    //Dashboard
    public function index()
    {
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        $admin = Admin::all();
        return view('admin.index', ['admin' => $admin], ['profile' => $profile]);
    }

    //Data Menara
    public function index2()
    {
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        $admin = Admin::all();
        return view('admin.index2', ['admin' => $admin], ['profile' => $profile]);

    }

    //Data Pemohon
    public function index3()
    {
        $user = User::findOrFail(session('id'));
        $profiles = Profile::findOrFail($user->id_profile);
        $profile = Profile::all();
        return view('admin.index3',['profile' => $profile ],['profiles' => $profiles ] );
    }

    //daftar permohonan
    public function index4()
    { 
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        $perusahaan = Permohonan::all();
        return view('admin.index4', ['permohonan' => $perusahaan], ['profile' => $profile]);
    }

    //data Akun User
    public function indexuser()
    {
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        $user = User::all();
        return view('admin.indexuser', ['user' => $user], ['profile' => $profile]);
    }
   
    //Create Konfirmasi Permohonan
    public function create(){
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        return view('admin.create', ['profile' => $profile]);
    }
    
    //Create Pemohon
    public function pemohon(){
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        $user = User::all();
        return view('admin.pemohon', ['user' => $user],['profile' => $profile]);
    }

    //Create Akun User
    public function createuser(){
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        return view('admin.createuser',['profile' => $profile]);
    }

    //database Menara
    public function data(Request $request){
        $validateData = $request->validate([
            'sitename'                     => 'required',
            'siteid'                       => 'required|min:3|max:100',
            'alamat'                       => 'required',
            'phone'                        => 'required',
            'image'                        => 'required|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $admin = new Admin();
        $admin->sitename = $validateData['sitename'];
        $admin->siteid = $validateData['siteid'];
        $admin->alamat = $validateData['alamat'];
        $admin->phone = $validateData['phone'];
        $admin->image = $validateData['image'];
        
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'image-'.time().".".$extFile;
            $path = $request->image->move('assets/files/image_admin', $namaFile);
            $admin->image = $path;
        }
        
        $admin->save();
        $request->session()->flash('pesan', 'Data berhasil ditambahkan');
        return redirect()->route('admin.index2');
    }
    
    public function updatemenara(Request $request, $admin){
        $validateData = $request->validate([
            'sitename'                     => 'required',
            'siteid'                       => 'required|min:3|max:100',
            'alamat'                       => 'required',
            'phone'                        => 'required',
            'image'                        => 'required|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $admin = Admin::findOrfail($admin);
        $admin->sitename = $validateData['sitename'];
        $admin->siteid = $validateData['siteid'];
        $admin->alamat = $validateData['alamat'];
        $admin->phone = $validateData['phone'];
        
        
        if ($request->hasFile('image')) {
            
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'image-'.time().".".$extFile;
            $path = $request->image->move('assets/files/image_admin', $namaFile);
            $admin->image = $path;
        }
        
        $admin->save();
        $request->session()->flash('pesan', 'Perubahan data berhasil');
        return redirect()->route('admin.showmenara', ['admin'=> $admin->id]);
    }

    //pemohon
    public function postdata(Request $request){
        $validateData = $request->validate([
            'username'                     => 'required',
            'sitename'                     => 'required',
            'alamat'                       => 'required',
            'tentang'                      => 'required',
            'phone'                        => 'required',
            'email'                        => 'required',
            'userEmail'                    => 'required',
            'image'                        => 'required|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $profile = new Profile();
        $profile->username = $validateData['username'];
        $profile->sitename = $validateData['sitename'];
        $profile->alamat = $validateData['alamat'];
        $profile->tentang = $validateData['tentang'];
        $profile->phone = $validateData['phone'];
        $profile->email = $validateData['email'];
        $profile->image = $validateData['image'];
        
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'image-'.time().".".$extFile;
            $path = $request->image->move('assets/files/image_pemohon', $namaFile);
            $profile->image = $path;
        }
        
        $profile->save();
        
        $updateUser = User::findOrFail($validateData['userEmail']);

        $updateUser->id_profile = $profile->id;

        $updateUser->push();

        $request->session()->flash('pesan', 'Data berhasil ditambahkan');
        return redirect()->route('admin.index3');
    }
    
    public function updatepemohon(Request $request, $profile){
            
            $validateData = $request->validate([
                'username'                     => 'required',
                'sitename'                     => 'required',
                'alamat'                       => 'required',
                'phone'                        => 'required',
                'image'                        => 'required|mimes:jpeg,png,jpg,gif,svg',
            ]);
        
            
            $profile = Profile::findOrfail($profile);
            $profile->username= $validateData['username'];
            $profile->sitename = $validateData['sitename'];
            $profile->alamat = $validateData['alamat'];
            $profile->phone = $validateData['phone'];
          
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'image-'.time().".".$extFile;
                $path = $request->image->move('assets/files/image_pemohon', $namaFile);
                $profile->image = $path;
            }
            
            $profile->save();
            $request->session()->flash('pesan', 'Data berhasil diupdate');
            return redirect()->route('admin.showpemohon',['profile'=> $profile->id]);
        }
        
        //update permohonan
        public function update(Request $request, $permohonan)
        {
            $validateData = $request->validate([
                'sitename'  => 'required',
                'status'    => 'required|in:Proses,Diterima,Ditolak',
            ]);
            
            $permohonan = Permohonan::findOrfail($permohonan);
            $permohonan->sitename = $validateData['sitename'];
            $permohonan->status   = $validateData['status'];
            
            $permohonan->save();
            
            $request->session()->flash('pesan', 'Permohonan Telah Diupdate');
            return redirect()->route('admin.show', ['permohonan'=> $permohonan->id]);
        }

        //User
        public function datauser(Request $request){
            $validateData = $request->validate([
                'name'                          => 'required',
                'email'                         => 'required',
                'password'                      => 'required|min:8',
                'is_admin'                      => 'required',
            ]);
            $user= new User();
            $user->name = $validateData['name'];
            $user->email = $validateData['email'];
            $user->password = Hash::make($validateData['password']);
            $user->is_admin = $validateData['is_admin'];
            
            $user->save();
            $request->session()->flash('pesan', 'Data berhasil ditambahkan');
            return redirect()->route('admin.indexuser');
        }

        public function updateuser(Request $request, $user)
        {
            $validateData = $request->validate([
                'name'                          => 'required',
                'email'                         => 'required',
                'password'                      => 'required',
                'is_admin'                      => 'required',
            ]);
            
            $user = user::findOrfail($user);
            $user->name = $validateData['name'];
            $user->email = $validateData['email'];
            $user->password = Hash::make($validateData['password']);
            $user->is_admin = $validateData['is_admin'];
            
            $user->save();
            
            $request->session()->flash('pesan', 'Permohonan Telah Diupdate');
            return redirect()->route('admin.showuser', ['user'=> $user->id]);
        }

        public function showuser($user_id){
            $user = User::findOrFail(session('id'));
            $profile = Profile::findOrFail($user->id_profile);
            $result  = user::findOrFail($user_id);
            return view('admin.showuser',['user' => $result], ['profile' => $profile]);
        }
        public function edituser($user_id){
            $user = User::findOrFail(session('id'));
            $profile = Profile::findOrFail($user->id_profile);
            $result = User::findOrFail($user_id);
            return view('admin.edituser',['user' => $result], ['profile' => $profile] );
        }

        public function deluser(Request $request, User $user){
            $user->delete();
            $request->session()->flash('pesan','Hapus data berhasil');
            return redirect()->route('admin.indexuser');
        }

        //profile Admin
        public function profil(){
            $user = User::findOrFail(session('id'));
            $profile = Profile::findOrFail($user->id_profile);
            return view('admin.profil', ['profile' => $profile]);
        }


        //permohonan
        public function show($permohonan_id){
            $user = User::findOrFail(session('id'));
            $profile = Profile::findOrFail($user->id_profile);
            $result = Permohonan::findOrFail($permohonan_id);
            return view('admin.show',['permohonan' => $result], ['profile' => $profile]);
        }
        
        public function edit($permohonan_id){
            $user = User::findOrFail(session('id'));
            $profile = Profile::findOrFail($user->id_profile);
            $result = Permohonan::findOrFail($permohonan_id);
            return view('admin.edit',['permohonan' => $result], ['profile' => $profile]);
        }
        
        public function delete(Request $request, Permohonan $permohonan){
            $permohonan->delete();
            $request->session()->flash('pesan','Hapus data berhasil');
            return redirect()->route('admin.index4');
        }
        
        //menara
        public function showmenara($admin_id){
            $user = User::findOrFail(session('id'));
            $profile = Profile::findOrFail($user->id_profile);
            $result = Admin::findOrFail($admin_id);
            return view('admin.showmenara',['admin' => $result],['profile' => $profile]);
        }
        
        public function editmenara($admin_id){
            $user = User::findOrFail(session('id'));
            $profile = Profile::findOrFail($user->id_profile);
            $result = Admin::findOrFail($admin_id);
            return view('admin.editmenara',['admin' => $result],['profile' => $profile]);
        }
        
        public function delmenara(Request $request, Admin $admin){
            $admin->delete();
            $request->session()->flash('pesan','Hapus data berhasil');
            return redirect()->route('admin.index2');
        }

        //pemohon
        public function showpemohon($profile_id){
            $user = User::findOrFail(session('id'));
            $profile = Profile::findOrFail($user->id_profile);
            $result = Profile::findOrFail($profile_id);
            return view('admin.showpemohon',['profile' => $result], ['profile' => $profile]);
        }
        
        public function editpemohon($profile_id){
            $user = User::findOrFail(session('id'));
            $profile = Profile::findOrFail($user->id_profile);
            $result = Profile::findOrFail($profile_id);
            return view('admin.editpemohon',['profile' => $result],['profile' => $profile]);
        }
        
        public function delpemohon(Request $request, Profile $profile){
            $profile->delete();
            $request->session()->flash('pesan','Hapus data berhasil');
            return redirect()->route('admin.index3');
        }
        
        
    }
    
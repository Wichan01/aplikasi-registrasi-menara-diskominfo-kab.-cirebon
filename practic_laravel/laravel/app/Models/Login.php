<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// class Login extends Model
// {
//     use HasFactory;
// }
class Login extends Authenticable
{
    use Notifiable;

    protected $guard = 'admin';

    protected $fillable = [
         'username', 'password'
    ];

    protected $hidden = ['password'];
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PermohonanController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::middleware('logincheck')->group(function () {
    // Permohonan
    Route::get('/permohonan/create', [PermohonanController::class, 'create'])->name('permohonan.create');
    Route::post('/permohonan', [PermohonanController::class, 'store'])->name('permohonan.store');
    Route::get('/permohonan/profil', [PermohonanController::class, 'profil'])->name('permohonan.profil');
    Route::get('/permohonan/index', [PermohonanController::class, 'index'])->name('permohonan.index');

    //Admin
    Route::get('/admin', [AdminController::class, 'index'])->name('admin.index');
    Route::get('/admin/create', [AdminController::class, 'create'])->name('admin.create');
    Route::get('/admin/profil', [AdminController::class, 'profil'])->name('admin.profil');
    Route::get('/admin/index2', [AdminController::class, 'index2'])->name('admin.index2');
    Route::get('/admin/index3', [AdminController::class, 'index3'])->name('admin.index3');
    Route::get('/admin/index4', [AdminController::class, 'index4'])->name('admin.index4');
    Route::get('/admin/pemohon', [AdminController::class, 'pemohon'])->name('admin.pemohon');
    Route::post('/admin/create', [AdminController::class, 'data'])->name('admin.data');
    Route::post('/admin', [AdminController::class, 'postdata'])->name('admin.postdata');

    Route::get('/admin/{permohonan}/show', [AdminController::class, 'show'])->name('admin.show');
    Route::get('/admin/{permohonan}/edit', [AdminController::class, 'edit'])->name('admin.edit');
    Route::patch('/admin/{permohonan}/edit', [AdminController::class, 'update'])->name('admin.update');
    Route::delete('/admin/{permohonan}', [AdminController::class, 'delete'])->name('admin.delete');

    Route::get('/admin/{admin}/showmenara', [AdminController::class, 'showmenara'])->name('admin.showmenara');
    Route::get('/admin/{admin}/editmenara', [AdminController::class, 'editmenara'])->name('admin.editmenara');
    Route::patch('/admin/{admin}/editmenara', [AdminController::class, 'updatemenara'])->name('admin.updatemenara');
    Route::delete('/admin/{admin}/delmenara', [AdminController::class, 'delmenara'])->name('admin.delmenara');

    Route::get('/admin/{profile}/showpemohon', [AdminController::class, 'showpemohon'])->name('admin.showpemohon');
    Route::get('/admin/{profile}/editpemohon', [AdminController::class, 'editpemohon'])->name('admin.editpemohon');
    Route::patch('/admin/{profile}/editpemohon', [AdminController::class, 'updatepemohon'])->name('admin.updatepemohon');
    Route::delete('/admin/{profile}/delpemohon', [AdminController::class, 'delpemohon'])->name('admin.delpemohon');

    Route::get('/admin/createuser', [AdminController::class, 'createuser'])->name('admin.createuser');
    Route::post('/admin/createuser', [AdminController::class, 'datauser'])->name('admin.datauser');
    Route::get('/admin/indexuser', [AdminController::class, 'indexuser'])->name('admin.indexuser');
    Route::get('/admin/{user}/showuser', [AdminController::class, 'showuser'])->name('admin.showuser');
    Route::get('/admin/{user}/edituser', [AdminController::class, 'edituser'])->name('admin.edituser');
    Route::patch('/admin/{user}/edituser', [AdminController::class, 'updateuser'])->name('admin.updateuser');
    Route::delete('/admin/{user}/deluser', [AdminController::class, 'deluser'])->name('admin.deluser');
});


Route::get('/login', [LoginController::class, 'login'])->name('login.login');
Route::post('/login', [LoginController::class, 'poslogin'])->name('login.poslogin');
Route::get('/logout', [LoginController::class, 'login'])->name('login.logout');
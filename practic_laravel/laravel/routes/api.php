<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PermohonanApiController;
use App\Http\Controllers\PermohonanController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('permohonan', [PermohonanApiController::class, 'store']);
Route::get('permohonan', [PermohonanApiController::class, 'index']);
// Route::get('/permohonan', [PermohonanController::class, 'index']);
// Route::get('/permohonan', [PermohonanController::class, 'create']);
